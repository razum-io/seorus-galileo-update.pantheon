
2006, July:
 * We now only print the "Thank you for signing the petition." message if
   there was no Thank You Page specified. We assume that if you've
   specified a Thank You Page, you've got your own verbiage you'd like
   to use and our default message is unwarranted.

 * The Thank You Page now defaults to '' instead of 'node'.
 * Removed configuration settings relating to the default Location Type.
 * Fixed a few strings that weren't translated, thanks to anarcat.

 * French and Spanish translations have been commited, thanks to.
   anarcat.The Spanish version appears out-of-date. Note that Trellon,
   LLC does not tech-support nor maintain these translations.

 * Heavily revised and cleaned version now available; CHANGELOG starts.
