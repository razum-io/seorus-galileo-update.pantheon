<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"
  <?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="ImageToolbar" content="false">
    <meta http-equiv="ClearType" content="true">    
    <meta name="MSSmartTagsPreventParsing" content="true">

  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

	<link href="/sites/all/themes/galilolel/styles/reset.css?B" media="all" rel="stylesheet" type="text/css">
  	<link href="/sites/all/modules/admin_menu/admin_menu.css?Q" media="all" rel="stylesheet" type="text/css">
  	<link href="/sites/all/modules/admin_menu/admin_menu-rtl.css?Q" media="all" rel="stylesheet" type="text/css">

  <?php print $styles; ?>

  	<script src="/sites/all/modules/jquery_update/replace/jquery.min.js?0" type="text/javascript"></script>
	<!-- 	<script src="http://cdn4.galilole.org.il/misc/drupal.js?0" type="text/javascript"></script>

  <?php print $scripts; ?>
  <!--[if IE 6]><link rel="stylesheet" href="<?php echo base_path() . $directory; ?>/style.ie6.css" type="text/css" media="screen" /><![endif]-->  
  <!--[if IE 7]><link rel="stylesheet" href="<?php echo base_path() . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>