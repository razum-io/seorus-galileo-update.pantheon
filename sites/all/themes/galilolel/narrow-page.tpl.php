
<?php print $page_top; ?>
<?php if (!empty($admin)) print $admin; ?>
	<div class="wrapper">
		<div class="container">
			
		<!--advertisments start-->
			<?php if(!empty($leftFloatingAd)){?><div id="leftFloatingAd"><?= $leftFloatingAd;?></div><?}?> 
		    <?php if(!empty($rightFloatingAd)){?><div id="rightFloatingAd"><?= $rightFloatingAd;?></div><?}?>
		<!--advertisments end-->
		
		    <?php if (!empty($topMenu)) {?>
			<?php	echo '<div id="topMenu">'.$topMenu; ?>

	        <?php echo '</div>'; } ?>
	       
			<div class="cleared" ></div>
			<div id="logo"></div>
			<div class="cleared" ></div>
			<?php if (!empty($mainMenu)) { echo '<div id="mainMenu">'.$mainMenu.'</div>'; } ?>
			<div class="cleared" ></div>
			<?php if (!empty($mainAd)) { echo '<div id="mainAd" >'.$mainAd.'</div>'; } ?>
			<div class="cleared" ></div>
		    <?php if (!empty($rightNavigation)) { echo '<div id="rightNavigation" >'.$rightNavigation.'</div>'; } ?>
		    
		    <div id="breadcrumbTabs" >
			    <?php if (!empty($help)) { echo $help; } ?>
				<?php if (!empty($messages)) { echo $messages; } ?>		    
		 	    <?php if (!empty($breadcrumb)) { echo $breadcrumb; } ?>
				<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
				<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
		  		<?php if (!empty($breadcrumbTabs)) { echo $breadcrumbTabs; } ?>
		    </div>
		    <div id="mainContent" class="narrowContent">
		   		<?php print render($content); ?>
		   </div>
		   <div class="cleared" ></div>
		   
		   <div id="footerArea">  <a href="http://tripleyou-websites.com/">Tripleyou WebSites</a>  
		   		<?php if (!empty($footer)) { echo '<div id="footer" >'.$footer.'</div>'; } ?>	
		   				   		
		   </div>
		   <div class="cleared" ></div>
		</div>
	</div>
	<!--[if lt IE 9]>
		<script src="http://cdn4.easysitenow.net/sites/all/themes/galilolel/roundIE.js"></script>
	<![endif]--> 
<?php echo $scripts ?>

<script src="http://cdn4.easysitenow.net/sites/all/themes/galilolel/script.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
//  _gaq.push(['_setAccount', 'UA-25695376-1']);
  _gaq.push(['_setAccount', 'UA-41413092-1']);
  _gaq.push(['_setDomainName', 'galilole.org.il']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68135664-1', 'auto');
  ga('send', 'pageview');

</script>
<?php print $page_bottom; ?>
